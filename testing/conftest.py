# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2015-2023 Tanguy Fardet
# SPDX-License-Identifier: CC0-1.0
# testing/conftest.py


def pytest_addoption(parser):
    parser.addoption(
        "--backends", action="store", default="networkx,igraph,graph-tool")
